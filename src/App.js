import './App.css';
import Header from './components/Header/Header';
import Home from './pages/Home';
import Loan from './pages/Loan';
import SpareTips from './pages/SpareTips';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <div>
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/lån">
              <Loan />
            </Route>
            <Route path="/sparetips">
              <SpareTips />
            </Route>
          </Switch>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
