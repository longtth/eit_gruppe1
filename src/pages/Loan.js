import React from "react";
import LoanBody from '../components/LoanBody/LoanBody'
import LoanBanner from '../components/LoanBanner/LoanBanner'

const Loan = ()  => (
    <div>
        <LoanBanner />
        <LoanBody />
    </div>
);

export default Loan;