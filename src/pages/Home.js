import React from "react";
import StockBanner from '../components/StockBanner/StockBanner';
import StockBody from '../components/StockBody/StockBody';

const Home = ()  => (
    <div>
        <StockBanner />
        <StockBody />
    </div>
);

export default Home;