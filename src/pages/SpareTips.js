import React from "react";
import SpareBody from '../components/SpareBody/SpareBody';
import SpareBanner from '../components/SpareBanner/SpareBanner';

const SpareTips = ()  => (
    <div>
        <SpareBanner />
        <SpareBody />
    </div>
);

export default SpareTips;