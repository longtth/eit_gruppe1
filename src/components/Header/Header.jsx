import styles from './Header.module.css';
import {Nav, Navbar} from 'react-bootstrap/';
import { NavLink } from "react-router-dom";

const Header = () => {
    return (
    <Navbar className={styles.header} expand="lg">
        <Navbar.Brand href="#home">Student hjelp</Navbar.Brand>
        <Nav className="mr-auto">
            {/* <div className={"container"}>Wrap buttons in here for hamburger menu</div> */}
            <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
            <Nav.Link as={NavLink} to="/lån" exact>Lån</Nav.Link>
            <Nav.Link as={NavLink} to="/sparetips" exact>Spare Tips</Nav.Link>
        </Nav>
    </Navbar>)
}

export default Header;