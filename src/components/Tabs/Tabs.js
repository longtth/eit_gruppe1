import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Tab from './Tabs';

const Tabs = () => {
    Tabs.propTypes = {
        children: PropTypes.instanceOf(Array).isRequired,
    }

    const [activeTab, setActiveTab] = useState(this.props.children[0].props.label);

    const {
        onClickTabItem,
        props: {
          children,
        },
        state: {
          activeTab,
        }
      } = this;


    return <div className="tabs">
    <ol className="tab-list">
      {children.map((child) => {
        const { label } = child.props;

        return (
          <Tab
            activeTab={activeTab}
            key={label}
            label={label}
            onClick={onClickTabItem}
          />
        );
      })}
    </ol>
    <div className="tab-content">
      {children.map((child) => {
        if (child.props.label !== activeTab) return undefined;
        return child.props.children;
      })}
    </div>
  </div>

}

const onClickTabItem = (tab) => {
        this.setState({ activeTab: tab });
      }