import danskebank from "./images/danskebank.png";
import dnb from "./images/dnb.png";
import sparebank1 from "./images/sparebank1.jpg";
import bn from "./images/BN-logo.png";

const LoanBanner = () => {
    return <div className="banner-container">
        <div className="logo-container">
            <a href="https://etilbudsavis.no/businesses/257bxm/publications/5o4kiIso/paged"><img className="logo" src={danskebank} alt="danskebank logo"/></a>
            <a href="https://www.dnb.no/"><img className="logo" src={dnb} alt="dnb logo"/></a>
            <a href="https://etilbudsavis.no/businesses/faa0Ym/publications/KHDxZ3NV/paged"><img className="logo" src={sparebank1} alt="sparebank1 logo"/></a>
            <a href="https://etilbudsavis.no/businesses/faa0Ym/publications/KHDxZ3NV/paged"><img className="logo" src={bn} alt="BNBank logo"/></a>
        </div>
    </div>
}

export default LoanBanner;