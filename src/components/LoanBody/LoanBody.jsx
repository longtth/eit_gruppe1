import Tabs from "../Tabs2/Tabs";
import LoanCalc from "../Calculator/LoanCalc";

const LoanBody = () => {
    
    return (
        <Tabs>
            <div label="Hva må man betale på et lån?"> 
                <h3>Hva må man betale på et lån?</h3>
                <LoanCalc />
                <div className="text-align-left">
                    <p>
                    På et lån inngår ofte båder renter og avdrag. Rentene er godtgjørelsen 
                    du betaler til den som har lånt deg pengene, slik at de også tjener noe 
                    på det. Avdraget er det du betaler ned på lånet ditt, slik at Renter du 
                    betaler ned på lån kan gi deg fradrag på skatten, ved at du kan trekke 
                    rentene fra inntekten din. Dersom du tjener 500 000 og betaler renter på 
                    200 000 i året, er inntekten du må skatte av kun 300 000.
                    </p>
                </div>
                <div className="text-align-left">
                    <p>
                        Som student kan du søke om å få avdragsfrie lån, da i all hovedsak huslån. 
                        Dette betyr at du i prinsippet kan utsette å betale ned på lånet ditt, og 
                        kun trenger å betale rentene på det.
                    </p>
                </div>
                <div className="text-align-left">
                    <p>
                        Du kan også velge hva slags rente du ønsker på lånet ditt, nemlig om du 
                        ønsker flytende eller fast rente. Fast rente settes for flere år, og er 
                        gjerne høyere for jo flere år du ønsker å låse den. Flytende rente følger 
                        markedsrenta, og vil ofte være lavere enn fast rente. Fordelen med fast rente 
                        er at du har forutsigbarhet, som f.eks i tider hvor den flytende renta skyter 
                        opp, hvor den faste renta kanskje blir lavere enn den flytende.
                    </p>
                </div>
            </div> 
            <div label="Studielån"> 
                <h3>Studielån</h3>
                <div className="text-align-left">
                    <p>
                        Som student ved godkjente universiteter eller høyskoler kan man ha rett på 
                        lån fra Statens Lånekasse. 40 prosent av lånet blir omgjort til stipend dersom 
                        man består utdanningen sin, og ikke bor med foreldrene sine. Noen studenter 
                        velger kun å søke om stipend.
                    </p>
                </div>
                <div className="text-align-left">
                    <p>
                        Studielånet er rente- og avdragsfritt så lenge du studerer, noe som vil si at 
                        lånet først koster deg noe når du er ferdig å studere. Dersom du har lest 
                        seksjonen Fond og Aksjer ser du kanskje da at det ikke er noen grunn til å ta 
                        opp maksimalt lån selv om du ikke har bruk for pengene? Siden lånet ikke koster 
                        deg noe, kan du med et studielån på f.eks. 100 000 kroner investere i et fond 
                        som på 5 år stiger 40%. Når du da må betale ned på lånet etter endt studietid 
                        sitter du igjen med 40 000kr etter du har betalt tilbake de 100 000 kronene du 
                        lånte. Det er såklart en risiko med i bildet når det gjelder aksjemarkedet, men 
                        dette er en god strategi å følge dersom man ser student og ikke trenger alle pengene.
                    </p>
                </div>
                <div className="text-align-left">
                    <p>
                        Avdrag og renter begynner du å betale når du er ferdig å studere, og man har 
                        muligheten til å utsette 36 ganger, altså 3 år totalt. Studielånet har en lav rente, 
                        og er billig sammenlignet med de aller fleste andre lån. Derfor lønner det seg å 
                        bruke så lang tid som mulig på å betale ned dette lånet dersom man har andre lån, 
                        og betale ned de dyrere lånene først. Vanlig (og maksimal) nedbetalingstid på 
                        studielånet er 20 år.
                    </p>
                </div>
                <div className="text-align-left">
                    <p>
                        Det er flere situasjoner som gir deg rett på sletting av studielån. Dette kan 
                        ting som å utdanne seg til yrker det er mangel på, noe som tidligere har vært 
                        tilfelle med læreryrket, eller om du flytter til et spesifikt sted i landet, som 
                        Finnmark eller Nord-Troms. Mer informasjon om hva som gjelder i dag finner du på 
                        nettsidene til Statens Lånekasse.
                    </p>
                </div>
                <div className="text-align-left">
                    <p>
                        For mer informasjon om studielånet, sjekk ut <a href="https://lanekassen.no/">Lånekassens hjemmeside</a> 
                    </p>
                </div>
            </div> 
            <div label="Forsikringer"> 
                <h3>Forsikringer</h3>
                <h4>Hva <u>må</u> man ha?</h4>
                <div className="text-align-left">
                    <p>
                        Man bør ha minimum innbo- og reiseforsikring. Dersom du er over 20 år er du ikke 
                        lenger dekket av dine foreldres forsikring, og dette gjelder også dersom du melder 
                        flytting til folkeregisteret og ikke er fylt 20 år. Det er også anbefalt å kjøpe 
                        reiseforsikring i tilfellet du skader deg på reise eller på vei hjem, eller til 
                        og med campus.
                    </p>
                </div>
                <h4>Hva lønner seg?</h4>
                <div className="text-align-left">
                    <p>
                        Velg forsikring ut fra prisen du har råd til og hva den inneholder. Hvis du for 
                        eksempel kjøper reiseforsikring er det lønnsomt å velge forsikring som dekker 
                        forsikring til flere i husstanden.  Er du student og ikke eier mange verdifull 
                        møbler og ting trenger du ikke å kjøpe den dyreste forsikringen. Kjøp heller 
                        forsikring som dekker det mest grunnleggende. Noen forsikringer tilbyr en form 
                        for best-garanti, at de dekker det alle andre forsikringer dekker. Da er du 
                        garantert at du ikke har en dårligere forsikring enn hva andre tilbyr.
                    </p>
                </div>
                <div className="text-align-left">
                    <p>
                        Det er også lønnsomt å kjøpe reiser med kredittkort fordi ofte trenger ikke man 
                        å betale ekstra for det, og her er det gjerne inkludert enkle forsikringer.
                    </p>
                </div>
            </div> 
        </Tabs>
    )
    
}

export default LoanBody;