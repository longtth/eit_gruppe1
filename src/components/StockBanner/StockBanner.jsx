import styles from './StockBanner.module.css';
import React, { useState } from 'react';

const StockBanner = () => {
    const [symbolList, setSymbolList] = useState([]);
    const [openList, setOpenList] = useState([]);
    const [changeList, setChangeList] = useState([]);
    const [fetched, setFetched] = useState(false);

    const fetchStockList = async () => {
        /* IBM */
        const API_KEY = "X78LJ5T77DV9D6QD";
        let SYMBOL = ["TSLA", "AAPL", "GOOGL", "GME"]
        /* This is the real fetch function
        fetch(`https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=${symbol}&apikey=${API_KEY}`) */

        SYMBOL.forEach(symbol =>
            fetch(`https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=IBM&apikey=demo`)
                .then(
                    (response) => {
                        return response.json();
                    }
                )
                .then(
                    (data) => {
                        const quote = data["Global Quote"];
                        const price = (Number(quote["02. open"])).toFixed(2);
                        const changePercent = (Number(quote["10. change percent"].slice(0, -1))).toFixed(2);
                        setChangeList(changeList => [...changeList, changePercent]);
                        setSymbolList(symbolList => [...symbolList, quote["01. symbol"]]);
                        setOpenList(openList => [...openList, price]);
                    }
                )
        );

        setFetched(true);
    }

    const checkChange = (change) => {
        const sign = change[0];
        if (sign === "-") {
            return (
                <span className={styles.downarrow}>&#8600;</span>
            );
        }
        return (
            <span className={styles.uparrow}>&#8599;</span>
        );

    }

    const populateBanner = (symbol, open, change) => {
        const item = symbol.map((x, i) => {
            return (
                <div className={styles.bannerItem}>
                    <h3>{x} {checkChange(change[i])}</h3>
                    <p>{open[i]} {change[i]}%</p>
                </div>
            );
        });
        return item;
    }


    React.useEffect(() => {
        fetchStockList();
    }, []);

    return <div className={styles.container}>
        {fetched ? populateBanner(symbolList, openList, changeList)
        :
        <div className={styles.bannerItem}>
            <h3>Oslo Børs <span className={styles.uparrow}>&#8599;</span></h3>
            <p>1,035.41 0.28%</p>
        </div>
        }

    </div>
}

export default StockBanner;