import React, { useState, useEffect } from "react";


const formatter = new Intl.NumberFormat("no-NO", {
  style: "currency",
  currency: "NOK",
  minimumFractionDigits: 2
});

const LoanCalc = () => {
  const [startingBalance, setStartingBalance] = useState(300000);
  const [monthlyContributions, setMonthlyContributions] = useState(200);
  const [timeToGrow, setTimeToGrow] = useState(5);
  const [growthMonthsOrYears, setGrowthMonthsOrYears] = useState("years");
  const [annualInterestRate, setAnnualInterestRate] = useState(8.99);
  const [compounds] = useState("monthly");
  const [calculation, setCalculation] = useState(0);
  const [calculation2, setCalculation2] = useState(0);

  const handleSubmit = e => {
    e.preventDefault();

    let percentInterestRate = annualInterestRate / 100;
    let numberOfCompoundings = compounds === "monthly" ? 12 : 365;
    let yearsToGrow =
      growthMonthsOrYears === "months" ? timeToGrow / 12 : timeToGrow;

    let base = 1 + percentInterestRate / numberOfCompoundings;
    let exponent = numberOfCompoundings * yearsToGrow;
    let compoundResult = Math.pow(base, exponent) * startingBalance;
    console.log(compoundResult);


    console.log("monthly contribution: " + monthlyContributions);
    console.log("years to grow: " + yearsToGrow);
    console.log("base: " + base);
    console.log("exponent: " + exponent);
    console.log("percent interest rate: " + percentInterestRate);
    console.log("number of compoundings: " + numberOfCompoundings);

    let futureValueResult =
      monthlyContributions *
      ((Math.pow(base, exponent) - 1) /
        (percentInterestRate / numberOfCompoundings));
    console.log("future value result: " + futureValueResult);


    var x = Math.pow(1+(percentInterestRate/12), yearsToGrow*12)
    // Term loans
    let calculation = formatter.format(
      (startingBalance*x*(percentInterestRate/12))/(x-1)
    );

    setCalculation(calculation);
    console.log("Final Result " + calculation);

    //Total
    let calculation2 = formatter.format(
      (startingBalance*x*(percentInterestRate/12))/(x-1)* (yearsToGrow*12)
    );

    setCalculation2(calculation2);
    console.log("Term amount " + calculation2);

  };



  return (
    <div className="compound-interest-calc" onSubmit={handleSubmit}>
      <form>
        <label>
          Lånebeløp: </label>
        <input
          type="text"
          id="starting-balance"
          value={startingBalance}
          onChange={event => setStartingBalance(event.target.value)}
        />
        <div>
          <label>
            Nedbetalingstid: </label>
          <input
            type="number"
            id="time-to-grow"
            value={timeToGrow}
            onChange={event => setTimeToGrow(event.target.value)}
          />
        </div>
        <div>
          <label>
            Effektiv rente:  </label>
          <input
            type="number"
            id="annual-interest-rate"
            step=".01"
            value={annualInterestRate}
            onChange={event => setAnnualInterestRate(event.target.value)}
          />

        </div>
        <input type="submit" value="Kalkuler terminbeløp og totalbeløp for lånet" />
      </form>
      <div>
        <label>Terminbeløp:</label><span>{calculation}</span>
      </div>
      <div>
        <label>Totalbeløp:</label><span>{calculation2}</span>
      </div>
    </div>
  );
};

export default LoanCalc;