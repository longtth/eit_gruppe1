import React, { useState, useEffect } from "react";


const formatter = new Intl.NumberFormat("no-NO", {
  style: "currency",
  currency: "NOK",
  minimumFractionDigits: 2
});

const CompoundInterestCalc = () => {
  // Add form hooks
  const [startingBalance, setStartingBalance] = useState(5000);
  const [monthlyContributions, setMonthlyContributions] = useState(200);
  const [timeToGrow, setTimeToGrow] = useState(1);
  const [growthMonthsOrYears, setGrowthMonthsOrYears] = useState("years");
  const [annualInterestRate, setAnnualInterestRate] = useState(0.35);
  const [compounds] = useState("monthly");
  const [calculation, setCalculation] = useState(0);

  const handleSubmit = e => {
    e.preventDefault();

    // Compound interest for principal formula
    // Amount = Principle * (1 + Rate/numberOfCompoundings)^(numberOfCompoundings * TimeInYears)
    let percentInterestRate = annualInterestRate / 100;
    let numberOfCompoundings = compounds === "monthly" ? 12 : 365;
    let yearsToGrow =
      growthMonthsOrYears === "months" ? timeToGrow / 12 : timeToGrow;

    let base = 1 + percentInterestRate / numberOfCompoundings;
    let exponent = numberOfCompoundings * yearsToGrow;
    let compoundResult = Math.pow(base, exponent) * startingBalance;
    console.log(compoundResult);

    // Future value of a series formula
    // PMT x p {[1 + r/n)^(nt) -1] / (r/n)}

    console.log("monthly contribution: " + monthlyContributions);
    console.log("years to grow: " + yearsToGrow);
    console.log("base: " + base);
    console.log("exponent: " + exponent);
    console.log("percent interest rate: " + percentInterestRate);
    console.log("number of compoundings: " + numberOfCompoundings);

    let futureValueResult =
      monthlyContributions *
      ((Math.pow(base, exponent) - 1) /
        (percentInterestRate / numberOfCompoundings));
    console.log("future value result: " + futureValueResult);

    // Compound interest for principal + Future value of a series
    let calculation = formatter.format(
      compoundResult + futureValueResult
    );

    setCalculation(calculation);
    console.log("Final Result " + calculation);
  };

  return (
    <div className="compound-interest-calc" onSubmit={handleSubmit}>
      <form>
        <label>
          Startsum: </label>
          <input
            type="text"
            id="starting-balance"
            value={startingBalance}
            onChange={event => setStartingBalance(event.target.value)}
          />
        <label>
          Månedlig sparebeløp: </label>
          <input
            type="text"
            id="monthly-contributions"
            value={monthlyContributions}
            onChange={event => setMonthlyContributions(event.target.value)}
          />
        <div>
        <label>
          Spareperiode: </label>
          <input
            type="number"
            id="time-to-grow"
            value={timeToGrow}
            onChange={event => setTimeToGrow(event.target.value)}
          />
          <label>
          <input
            type="radio"
            value="years"
            checked={growthMonthsOrYears === "years"}
            onClick={() => setGrowthMonthsOrYears("years")}
          />
           År
          </label>
          <label>
          <input
            type="radio"
            value="months"
            checked={growthMonthsOrYears === "months"}
            onClick={() => setGrowthMonthsOrYears("months")}
          />
          Måneder
        </label>
        </div>
        <div>
          <label>
            Sparerente:  </label>
            <input
              type="number"
              id="annual-interest-rate"
              step=".01"
              value={annualInterestRate}
              onChange={event => setAnnualInterestRate(event.target.value)}
            />

        </div>
        <input type="submit" value="Kalkuler totalbeløp for perioden" />
      </form>
      <div>
        <label>Totalbeløp:</label><span>{calculation}</span>
      </div>
    </div>
  );
};

export default CompoundInterestCalc;