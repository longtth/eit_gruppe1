import Tabs from "../Tabs2/Tabs"
import aksjesparekonto from "../images/aksje_fond/aksjesparekonto.png"

const StockBody = () => {
    
    return (
        <Tabs>
            <div label="Aksjesparekonto"> 
                <h3>Hva er en aksjekonto?</h3>
                <img className="" src={aksjesparekonto} alt="aksje spare konto"/>
                <p>
                    Enten du er ny i aksjemarkedet, eller en gammel traver, er aksjesparekonto et “must-have” for deg.
                    I Norge kan du nemlig ikke handle aksjer eller andre verdipapirer fra vanlige kontoer
                    som brukskonto eller sparekonto, og du trenger derfor egne kontoer for dette.
                    Aksjesparekonto ble innført i Norge i 2017, og har etter det blitt den vanligste type konto for aksjehandel.
                </p>
                <p>
                    I Norge må man skatte 32% av all gevinst på aksjehandel. En aksjesparekonto skiller seg fra andre de gamle
                    handelskontoene (som f.eks. Aksje- og fondskonto) ved at den tilbyr skattefritt kjøp og salg av verdipapirer,
                    inntil du velger å ta ut gevinst fra kontoen. Dette vil i praksis bety at du kan fortsette å oppnå avkastning
                    på pengene dine ved flere kjøp og salg, og du trenger ikke skatte før du til slutt bestemmer deg for at pengene
                    skal ut fra aksjesparekontoen.
                </p>
                <p>
                    Hvorfor er det gunstig med en aksjesparekonto? Se for deg at du setter inn 10 000kr på aksjesparekontoen,
                    og kjøper aksjer i Netflix for disse pengene. Deretter stiger Netflix 50%, du selger aksjene dine, og
                    sitter igjen med 15 000kr på kontoen. Nå har du lyst til å kjøpe aksjer i Kahoot for disse 15 000 kronene.
                    Dersom du måtte skattet 32% av gevinsten din på 5 000kr fra Netflix ville du kun hatt 13 400kr å kjøpe aksjer
                    for i Kahoot, men ettersom du har en aksjesparekonto kan du kjøpe aksjer for alle 15 000kr.
                    Som du ser fikk du flere penger å kjøpe aksjer i Kahoot for når du utsatt skatten.
                    Dersom du har lyst til å kjøpe og selge flere aksjer over lengre tid, ser du kanskje at det vil lønne seg
                    å betale skatt til slutt? Skatten må først betales den dagen du velger å ta ut pengene fra aksjesparekontoen,
                    og da må du betale skatt på gevinsten du har oppnådd, altså alt over de 10 000 kronene du startet med. 
                </p>
                <p>
                    En aksjesparekonto består av tre underkontoer. Én for å holde kontantene du har på kontoen, én underkonto
                    som holder fondene dine, og én underkonto for å holde aksjene dine.
                </p>
                <p>
                    Dersom du har lyst til å lære enda mer om hva en aksjesparekonto er, sjekk ut <a href="https://aksjenorge.no/aksjesparing/aksjesparekonto/ofte-stilte-sporsmal-svar/">Aksjenorge.no</a> og <a href="https://www.skatteetaten.no/bedrift-og-organisasjon/rapportering-og-bransjer/tredjepartsopplysninger/bank-finans-og-forsikring/aksjesparekonto/om-aksjesparekonto/">Skatteetaten</a>! 
                </p>
            </div> 
            <div label="Hva er en aksje?"> 
                <h3>Hva er en aksje?</h3>
                <p>
                    En aksje er i all hovedsak en eiendel av et selskap. Et selskap er gjerne delt opp i flere biter som kan eies,
                    alt fra én bit til mange millioner. Hver av disse bitene kalles aksjer, og jo flere aksjer du eier, jo større eierandel har du i selskapet. 
                </p>
                <p>
                    Svært mange selskap er ikke børsnotert, altså er det ikke mulig for offentligheten å kjøpe aksjer i dem.
                    Et eksempel på dette kan være den lokale skoforhandleren i hjembyen din, eller frisøren over gata.
                    Her er det gjerne få eiere, som f.eks. fire familiemedlemmer som hver eier 25% av aksjene.
                </p>
                <p>
                    Det stilles visse krav til et selskap for å kunne gå på børsen og gjøre aksjene sine tilgjengelige for offentligheten.
                    Vi vil ikke gå inn på kravene her, men disse kan finnes på <a href="https://lovdata.no/dokument/NL/lov/1997-06-13-45 ">lovdata</a>.
                </p>
                <p>
                    Når du eier en andel av et selskap i form av aksjer, eier du også en andel av den totale verdien til selskapet.
                    Se for deg at 10% av en samling med frimerker, som totalt kan selges for 10 000kr. Da er din andel verdt 1 000kr.
                    Det samme gjelder for selskaper på børsen, hvor dine aksjer representerer verdi i selskapet.
                    Prisen på én aksje defineres derfor ut fra hvor mange deler (aksjer) selskapet skal deles opp i, og hvor mye selskapet er verdt.
                    Dersom selskapet som helhet øker i verdi, øker også alle aksjene i verdi. Dersom selskapet blir mindre verdt, som f.eks.
                    dersom det viser seg at selskapet ikke kommer til å tjene like mye penger, vil aksjene synke i verdi.
                </p>
                <p>
                    Et praktisk eksempel: la oss si at Flyselskapet ASA er verdt 10 000 000kr, og ønsker å utstede 2 000 000 aksjer.
                    Her vil hver aksje være verdt 5kr, slik at den totale summen av alle aksjer er like mye verdt som selskapet.
                    Dersom Flyselskapet ASA øker verdien av selskapet til 15 000 000kr, vil hver aksje nå være verdt 7,5kr.
                </p>
                <p>
                    Spørsmålet til slutt er så klart hvordan man vet hvor mye et selskap er verdt, og hvordan man kan gjette på hva prisen
                    til et selskap vil være om 1 år. Dette er hva alle investorer, private og institusjonelle, prøver å få til, og som svært få klarer.
                    Det finnes utallige analyser og metoder man kan bruke for å regne på dette. For mange er nyhetsbildet og troen på selskaper et godt utgangspunkt!
                </p>
            </div> 
            <div label="Hva er fond?"> 
                <h3>Hva er fond?</h3>
                <p>
                    Dersom du ikke vet hva en aksje er, anbefaler vi at du leser om det her før du leser om fond.
                    Et fond er en sammensetning av forskjellige aksjer, hvor utviklingen til alle de underliggende aksjene utgjør utviklingen til fondet.
                    La oss tenke oss et fond med 10 selskaper, hvor hvert selskap utgjør 10% av fordelingen på fondet. Dersom alle 10 selskapene stiger
                    med 100% stiger fondet med 100%, men dersom kun 5 av selskapene stiger med 100% (og resten holdes på 0%), vil fondet stige 50%.
                    Du kan kanskje tenke deg at et fond som består av mange forskjellige aksjer utgjør mindre risiko enn å kun være investert i ett selskap?
                </p>
                <p>
                    Et fond forvaltes gjerne av en bank, enten passivt eller aktivt. Dersom et fond er passivt vil det si at ingen jobber daglig med å kjøpe
                    og selge aksjer i fondet, og fondet består av et sett med aksjer som ikke byttes ut på en god periode. Det vanligste passive fondet kalles indeksfond,
                    og er fond som inneholder de X største selskapene på en børs. Dette vil si at dersom børsen i sin helhet stiger, vil også dette fondet stige.
                    Her kan man for eksempel velge mellom globale og nasjonale indeksfond.
                </p>
                <p>
                    Et aktivt fond er et fond som aktivt forvaltes av en eller flere personer, hvor jobben deres er å kjøpe og selge aksjer i dette fondet.
                    Disse fondene spesialiserer seg ofte på bransjer eller sektorer som f.eks. teknologi, bærekraft, medisin osv. Disse fondene er dermed noe mer risikable
                    enn indeksfond, men kan til gjengjeld gi en bedre avkastning. Årsaken til at de ofte er mer risikable enn indeksfond er fordi et aktivt fond ofte
                    er eksponert for én sektor, mens indeksfond er eksponert mot børsen i sin helhet.
                </p>
                <p>
                    Fond har årlige forvaltningshonorarer, en kostnad forvalterne tar for å forvalte pengene dine. Aktive fond har naturligvis høyere honorarer enn passive fond,
                    og disse honorarene ligger som oftest på 0,15 - 2% årlig. Dette er så klart noe å være oppmerksom på når du skal velge deg fond!
                </p>
            </div>
            <div label="Børsmarkedet">
                <h3>Børsmarkedet</h3>
                <p>
                    En indeks viser verdiutviklingen for et sett med aksjer, gjerne for et spesielt marked. Man har en indeks for Oslo Børs som består av de største
                    aksjene på <a href="https://no.wikipedia.org/wiki/OSEBX-indeksen">Oslo Børs (OSEBX)</a>, og man har indekser for for eksempel teknologi
                    i <a href="https://no.wikipedia.org/wiki/NASDAQ">USA (NASDAQ)</a>. Indekser som disse fungerer gjerne som representanter for hele aksjemarkedet,
                    og utgjør også det vi snakket om under indeksfond.
                </p>
                <p>
                    Børsmarkedet har over en lengre periode (long term) vært stigende. Det er mye svingninger underveis,
                    men som det kan sees i grafen under stiger markedet over lengre tid. 
                </p>
            </div>
            <div label="Hvordan handle aksjer og fond">
                <h3>Hvordan handle aksjer og fond</h3>
                <p>
                    Aksjer og fond kan handles hos de fleste banker. De største aktørene i Norge, med det beste utvalget, er Nordnet og DNB.
                    Begge har gode nettsider for kjøp og salg, i tillegg til brukervennlige apper. DNB har utviklet en egen app for sparing i aksjer og fond,
                    nemlig DNB Spare, som gjør det gøy å begynne spare og følge med på utviklingen. 
                </p>
                <p>
                    I det siste har det kommet flere nye aktører på banen som søker å gjøre det enkelt (og gøy) for unge å spare i fond,
                    og her er Kron den mest kjente. Dette er et godt alternativ for deg som ikke ønsker å legge mye energi i det,
                    men dersom du har lyst til å sette deg litt inn i fondshandel vil det i de fleste tilfellene være mer lukrativt å velge egne fond hos en bank.
                </p>
            </div>
            <div label="Hva bør jeg kjøpe?">
                <h3>Hva bør jeg kjøpe?</h3>
                <p>
                    Det er umulig å gi tips om hva som lønner seg å kjøpe, da markedet alltid er i forandring, og fordi det er umulig å spå fremtiden.
                    Det er dog én ting som går igjen hos de fleste investorer og økonomer: indeksfond er en trygg havn dersom du er ny i markedet.
                    Indeksfond følger indeksen, og du er sikret en relativt lik avkastning som markedet. Som nevnt under børsmarkedet stiger markedet “long-term”,
                    og dersom du sparer over lengre tid er det stor sannsynlighet for at du sitter igjen med gevinst i indeksfond.
                </p>
                <p>
                    Dersom du ønsker litt mer risiko er aktive fond et godt alternativ til indeksfond, eller for de fleste: et godt supplement til indeksfond.
                    Det siste året har enkelte aktive fond steget over 100%, noe som er høyst uvanlig, men dette representerer mulighetene dersom du treffer riktig bransje/fond.
                    I disse tilfellene vil du være glad for at du valgte dette fremfor indeksfond, men i andre tilfeller vil indeksfond score bedre enn aktive fond.
                </p>
            </div>
            <div label="Spare i fond">
                <h3>Spare i fond</h3>
                <h4><i>Time in the market beats timing the market</i></h4>
                <p>
                    For fond vil det å være i markedet over tid i de aller fleste tilfeller slå det å prøve og kjøpe seg inn på et billig tidspunkt.
                    Det er umulig å vite om toppen som nå gjelder egentlig er et lavt punkt på veien mot en høyere topp,
                    eller om bunnen man nå er i til slutt viser seg å være et mye høyere punkt enn bunnen som kommer senere.
                    Ved å tenke langsiktig, vet man at aksjemarkedet mest sannsynlig har stiget, og man bryr seg ikke om svingningene på veien. 
                </p>
                <p>
                    I tillegg er en svært nyttig metode for å oppnå en god pris på det du kjøper, å spare jevnlig.
                    Dermed får du med deg alle topper og bunner, og oppnår et godt gjennomsnitt på alle kjøpene dine.
                    Dette kalles “dollar cost averaging” (DCA). Istedenfor å kjøpe én gang (som viser seg å være en topp),
                    prøve å time markedet på en bunn (som også viser seg å være en topp), kan du spare over tid og få med deg alle opp- og nedturer.
                </p>
            </div>
            <div label="Skatt">
                <h3>Skatt</h3>
                <p>
                    Som nevnt tidligere skatter man 32% av all gevinst i Norge. For aksjehandel gjennom kontoer i norske banker registreres kjøp,
                    salg og uttak automatisk, slik at dette kommer med i skatteberegningen din. Som alltid er det så klart lurt å dobbeltsjekke dette.
                </p>
                <p>
                    Dersom du uheldigvis sitter igjen med et tap i aksjehandel, er du så heldig å få fratrekk på skatten din.
                    32% av tapet ditt vil kunne trekkes fra på skatten din, slik at tapet heldigvis minimeres.
                </p>
            </div>
        </Tabs>
    )
    
}

export default StockBody;