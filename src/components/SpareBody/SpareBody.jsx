import Tabs from "../Tabs2/Tabs";
import CompoundInterestCalc from "../Calculator/CompoundInterestCalc";

const SpareBody = () => {
    
    return (
        <Tabs>
            <div className="tab-content" label="Våres topp 10 sparetips">
                <h3>Våres topp 10 sparetips</h3>
                <div className="text-align-left">
                    <ol>
                        <li>Sett opp et budsjett for måneden, og før regnskap på utgifter.</li>
                        <li>Planlegg middager ut fra hva det er tilbud på, og hva du har i kjøleskapet.</li>
                        <li>Handle så få ganger i uka som mulig, helst én.</li>
                        <li>Spør alltid om studentrabatt!</li>
                        <li>Ha et månedlig sparebeløp du ønsker å sette av til noe du ønsker deg, eller bare for fremtidig sparing.</li>
                        <li>Vent på tilbud om du skal kjøpe noe nytt.</li>
                        <li>Sjekk om det finnes billigere brukt om du skal kjøpe noe nytt.</li>
                        <li>Hør med venner om du kan låne av dem før du kjøper det.</li>
                        <li>Sjekk kilopris/månedspris osv. når du kjøper noe. Kanskje månedsbillett på buss lønner seg for deg?</li>
                        <li>Tenk deg om to ganger før du kjøper noe!</li>
                    </ol>
                </div>
            </div> 
            <div className="tab-content" label="Budsjett"> 
                <h3>Budsjett</h3>
                <div className="text-align-left">
                    <p>
                        Noe av det smarteste du kan gjøre som student med stram økonomi, er å sette opp et enkelt studentbudsjett. Et budsjett handler først og fremst om to ting: hvor mye som kommer inn, og hvor mye som går ut. Når du har fått en god oversikt over hvor mye du får inn, er det enkelt å legge seg på et utgiftsnivå som passer deg.
                    </p>
                    <h4><i>Eksempel på studentbudsjett:</i></h4>
                    <h4>Penger inn</h4>
                    <p>
                        Her kan man føre alle inntektene som kommer inn i løpet av et semesteret. Her er typisk inntekt lån og stipend, og eventuelt fra en deltidsjobb hvis du har det.
                    </p>
                </div>
                <h4>Penger ut</h4>
                <div className="text-align-left">
                    <ul style={{listStyle: "none"}}>
                        <li><u>Engangsutgifter</u></li>
                        <li>Bøker og skolemateriell</li>
                        <li>Halvårskort på treningssenter</li>
                        <li>Semesteravgift</li>
                        <li>&nbsp;</li>
                        <li><u>Månedsutgifter:</u></li>
                        <li>Husleie </li>
                        <li>Mat </li>
                        <li>Strøm </li>
                        <li>Mobil </li>
                        <li>Busskort</li>
                        <li>Sparing/buffer </li>
                    </ul>
                </div>
            </div> 
            <div className="tab-content" label="Tips for å spare på mat"> 
                <h3>Tips for å spare på mat</h3>
                <div className="text-align-left">
                   <p>
                    Planlegging før mathandling er nødvendig for å kunne spare mest mulig på matposten, og her er noen nyttige tips:
                </p>
                <ol>
                    <li>Planlegg måltidene dine og sett opp matplan for uken</li>
                    <li>Ha rester i din matplan, og bruk det du har hjemme.</li>
                    <li>Skriv handleliste og hold deg til den </li>
                    <li>Prøv å handle en gang i uken</li>
                    <li>Bruk nettsider og apper til å finne mattilbuder og oppskrifter</li>
                    <li>Ikke handle mat når du er sulten</li>
                    <li>Sjekk varer som plassert nederst. Billigere matvarer ligger ofte nederst, og det dyreste ligger i hodehøyde.</li>
                    <li>Sjekk alltid kilopris versus den totale prisen på en vare. Det er billigere å kjøpe varer med billigere kilopris.</li>
                </ol> 
                </div>
            </div>
            <div className="tab-content" label="Kundefordeler og studentrabatter">
                <h3>Kundefordeler og studentrabatter</h3>
                <div className="text-align-left">
                    <p>
                        Det er mange gode studentrabatter der ute. Selv om det ikke reklameres direkte for det, er det stor mulighet for at butikken du skal kjøpe noe i har studentrabatt. Derfor er det alltid lurt å spørre om det. Du kommer til å bli overrasket over hvor mye du kan spare på det. Spis, gå på kino, klipp håret eller reis billigere med studentrabatter.
                    </p>
                    <p>
                        Andre nyttige tips:
                        <ol>
                            <li>Det lønner seg å melde seg som kunde på ulike butikker og følge med på med tilbud hos dem. </li>
                            <li>Det er salg omtrent hele året. Det er fordi ulike produkter er på salg til forskjellige tider og er derfor lurt dersom du skal kjøpe nytt og benytte av deg salget for å spare mest mulig.</li>
                            <li>Kombiner gjerne salg med kuponger eller/og studentrabatter.</li>
                            <li>Bruk studentrabatter for å kjøpe bussbilletter/billetter både lokalt og rundt i landet.</li>
                        </ol>
                    </p>
                    
                    
                    <p>
                        <b>Studentrabatter</b>
                        <ul style={{listStyle: "none"}}>
                            <li><a href="https://studentkortet.no/rabatter">Studentkortet</a></li>
                            <li><a href="https://studenttorget.no/studentrabatter">Studenttorget</a></li>
                        </ul>
                    </p>  
                </div>
            </div>
            <div className="tab-content" label="Gjenbruk">
                <h3>Gjenbruk</h3>
                <div className="text-align-left">
                <p>
                        Nå har gjenbruk blitt moteriktig og kult og ikke minst sparer du en del penger som student på det.Kjøp lærebøkene, klær og andre ting du har behov for brukt og finn de beste tingene på Tise eller Finn. 
                </p>
                <p>
                    <b>Gjenbruk</b>
                    <ul style={{listStyle: "none"}}>
                        <li><a href="www.finn.no">Sirkula</a></li>
                        <li><a href="www.tise.com">Tise</a></li>
                    </ul>
                </p>
                </div>
 
            </div>
            <div className="tab-content" label="Låne/leie ting og tang">
                <h3>Låne/leie ting og tang</h3>
                <div className="text-align-left">
                    <p>
                        Som student er det mange steder man kan spare penger, og det å låne/leie ting er helt klart et viktig punkt. Det finnes mye man kun trenger én eller få ganger, og det å skulle kjøpe denne tingen vil i mange tilfeller være svært ugunstig. Det å spørre rundt om det er noen man kjenner som låner bort noe vil være en god måte å spare penger på. Dette kan være ting som f.eks. krumkakejern til julebaksten eller drill til å fikse noe hjemme.
                    </p>
                    <p>
                        For andre, større ting som f.eks. bil vil det være så klart ikke la seg gjøre å kjøpe (eller enkelt låne av bekjente) og da vil leie være gunstig. Her gjelder det å være obs på hvor man kan leie dette, og sjekke forskjellen på priser. De typiske tingene å leie for studenter inkluderer bil og klær, og da ofte på Nabobil eller Fjong (nabobil.no, fjong.com). I tillegg kan man til og med få tak i gratis bil dersom det passer med reiseruten din, ved å hjelpe til med å flytte biler på returbil.no.
                    </p>
                </div>
                
                <p>Her er eksempler på steder du kan leie ting og tang:<br></br></p>
                <b>Bil:</b>
                <ul style={{listStyle: "none"}}>
                    <div className="text-align-left">
                        <li><a href="www.nabobil.no">Nabobil</a></li>
                        <li><a href="www.trondheim-bilkollektiv.no">Trondheim bilkollektiv</a></li>
                        <li><a href="https://www.rent-a-wreck.no/">Rent-a-wreck</a></li>
                        <li><a href="www.returbil.no">Returbil</a></li>
                    </div>
                </ul>
                <b>Klær:</b>
                <ul style={{listStyle: "none"}}>
                    <li><a href="www.fjong.com">Fjong</a></li>
                    <li><a href="www.hygglo.no">Hygglo</a></li>
                </ul>
                <b>Andre ting:</b>
                <div className="text-align-left">
                    <ul style={{listStyle: "none"}}>
                        <li><a href="www.leieting.no">Leieting</a></li>
                        <li><a href="www.hygglo.no/">Hygglo</a></li>
                        <li><a href="www.sirkula.no">Sirkula</a></li>
                    </ul>
                </div>
            </div>
            <div className="tab-content" label="Mattilbud og oppskrifter">
                <h3>Mattilbud og oppskrifter</h3>
                <p>Her skal det havne en funksjon som henter inn tilbud og finner oppskrifter basert på disse. Blir selvfølgelig statisk i vår nettside :) </p>
                <p>
                    <b>Tilbudsaviser</b>
                    <ul style={{listStyle: "none"}}>
                        <li><a href="www.matprat.no">Matprat</a></li>
                        <li><a href="https://etilbudsavis.no/">Etilbudsavis</a></li>
                    </ul>
                    <b>Blogger og oppskrifter</b>
                    <ul style={{listStyle: "none"}}>
                        <li><a href="https://www.instagram.com/fattig.student/?hl=nb">Instagram @fattig.student</a></li>
                        <li><a href="https://trinesmatblogg.no/">Trines matblogg</a></li>
                        <li><a href="https://www.matprat.no/artikler/tema/studentmat-priser-pa-retter/">Matprats studentretter</a></li>
                        <li><a href="https://www.bbcgoodfood.com/recipes/collection/student-recipes">BBCs studentretter</a></li>
                        <li><a href="https://studentrecipes.com/">Student recipes</a></li>
                    </ul>
                    <b>Oppskrifter basert på tilbudsvarer</b><br></br>
                    
                    
                </p>
            </div>
            <div className="tab-content" label="Sparekalkulator">
                <CompoundInterestCalc />
            </div>
        </Tabs>
    )
    
}

export default SpareBody;