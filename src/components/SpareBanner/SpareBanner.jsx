import bunnpris from "./images/bunnpris-logo.png";
import kiwi from "./images/kiwi_logo.jpg";
import rema from "./images/rema-logo.png";
import meny from "./images/meny-logo.jpg";

const SpareBanner = () => {
    return <div className="banner-container">
        <div className="logo-container">
            <a href="https://etilbudsavis.no/businesses/257bxm/publications/5o4kiIso/paged"><img className="logo" src={kiwi} alt="kiwi logo"/></a>
            <a href="https://etilbudsavis.no/businesses/5b11sm/publications/TZf3lDgq/paged"><img className="logo" src={bunnpris} alt="bunnpris logo"/></a>
            <a href="https://etilbudsavis.no/businesses/faa0Ym/publications/KHDxZ3NV/paged"><img className="logo" src={rema} alt="rema logo"/></a>
            <a href="https://etilbudsavis.no/businesses/4333pm/publications/ncIQ52X3/paged"><img className="logo" src={meny} alt="meny logo"/></a>
        </div>
    </div>
}

export default SpareBanner;